import by.gstu.diploma.drying.services.Configuration;
import by.gstu.diploma.drying.services.DynamicCalculator;
import by.gstu.diploma.drying.services.Randomizer;
import by.gstu.diploma.drying.services.RandomizerBuilder;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Random;

import static com.mongodb.internal.connection.tlschannel.util.Util.assertTrue;

@Ignore
public class DynamicCalculatorTest {

	@BeforeClass
	public static void initConfig() {

//		Configuration cfg = Configuration.getInstance();
//
//		cfg.setMaxTempHeatSand(52.0);
//		cfg.setLambda(50.0);
//		cfg.setTempHeatbearerAtEntrance(130.0);
//		cfg.setLowestHeatFuelAbility(8050.0);
//		cfg.setSandHumidityBeforeDrying(21.0);
//		cfg.setSandHumidityAfterDrying(14.0);
//		cfg.setFuelType("Природный газ", some -> System.out.println(""));
//		cfg.setZeroHumidity("70", some -> System.out.println(""));
//		cfg.setZeroTemp("15", some -> System.out.println(""));
//		cfg.setDryerParams("Сушилка барабанная стационарная СЗСБ-8,0А", some -> System.out.println(""));
	}

	@Test
	public void sandCurrentProcessedAmmountTest() throws InterruptedException {
		try {
			Thread.sleep(new Random().nextInt(3000) + 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertTrue(true);
//		RandomizerBuilder builder = RandomizerBuilder.getBuilder();
//
//		Randomizer rand = builder.build();
//
//		DynamicCalculator dyCalc = new DynamicCalculator(rand);
//		int timeRate = 1;
//		for(int i = 0; i < 10; i ++) {
//			System.out.println(String.format("Current ammount %.2f kg", dyCalc.getTotalSandProcessed(timeRate)));
//			Thread.sleep(timeRate * 1000);
//		}
	}

	@Test
	public void requiredDryerPerformanceTest() throws InterruptedException {
		try {
			Thread.sleep(new Random().nextInt(3000) + 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertTrue(true);
//		RandomizerBuilder builder = RandomizerBuilder.getBuilder();
//
//		Randomizer rand = builder.build();
//
//		DynamicCalculator dyCalc = new DynamicCalculator(rand);
//
//		int timeRate = 1;
//		for(int i = 0; i < 100; i ++) {
//			System.out.println(String.format("Required performance %.2f", dyCalc.getCurrentDryerPerformance()));
//		}
	}

	@Test
	public void normalDistributionTest() {

		try {
			Thread.sleep(new Random().nextInt(3000) + 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		assertTrue(true);
	}

	@Test
	public void someAnotherTest() {
		int from = 5;
		int to = 2;

		System.out.println(String.format("from %d to %d", from, to));
		from = to + from;
		to = from - to;
		from = from - to;

		System.out.println(String.format("from %d to %d", from, to));
//		int res = 0;
//		for(int i = 0; i < 100; i ++) {
//			double next = 0;
//			do {
//				next = new Random().nextGaussian();
//				if (next < 0)
//					next = next * -1;
//			} while(next*100 < 0 || next*100 > 99);
//			System.out.println(String.format("%.0f", next * 100));
//		}
	}

}
