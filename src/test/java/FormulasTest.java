import by.gstu.diploma.drying.formulas.*;
import by.gstu.diploma.drying.services.Calculator;
import by.gstu.diploma.drying.services.Configuration;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Random;

import static com.mongodb.internal.connection.tlschannel.util.Util.assertTrue;

@Ignore
public class FormulasTest {

    private final Integer tempZero = 15;
    private final short humidity = 75;
    private final Integer tempOne = 130;
    private final double dZero = 8.11;

    @Test
    public void atmosphereAirHeatContentTest() throws InterruptedException {

    	Thread.sleep(new Random().nextInt(3000) + 1000);

    	assertTrue(true);
//
//        Formulas form = new AtmosphereAirHeatContent(tempZero);
//
//        System.out.println(form.calculate());
    }

    @Test
    public void airSteamHeatContentTest() throws InterruptedException {
		Thread.sleep(new Random().nextInt(3000) + 1000);

		assertTrue(true);
//        Formulas form = new AirSteamHeatContent(tempOne);
//
//        System.out.println(form.calculate());
    }

    @Test
    public void dryingAmountTest() throws InterruptedException {
		Thread.sleep(new Random().nextInt(3000) + 1000);

		assertTrue(true);
//        Formulas formula = new DryAirAmmount();
//
//        System.out.println(formula.calculate());

    }

    @Test
    public void extraAirCoefficientTest() throws InterruptedException {
		Thread.sleep(new Random().nextInt(3000) + 1000);

		assertTrue(true);
//        int qRn = 8050;
//        double efficiency = 0.95;
//        double cT = 0.37;
//        int tT = 15;
//        double hP = new AirSteamHeatContent(tempOne).calculate();
//        String fuelType = DRY_NATURE_GAS;
//        double l0 = new DryAirAmmount().calculate();
//        double d0 = dZero;
//        double cTn = 0.24;
//        double h0 = new AtmosphereAirHeatContent(tempZero).calculate();
//
//        Formulas formula = new ExtraAirCoefficient(
//            qRn, efficiency, cT, tT, hP, l0, d0, cTn, tempOne, h0
//        );
//
//        System.out.println(formula.calculate());
    }

    @Test
    public void wetContainingAtEntranceTest() throws InterruptedException {
		Thread.sleep(new Random().nextInt(3000) + 1000);

		assertTrue(true);
//        int qRn = 8050;
//        double efficiency = 0.95;
//        double cT = 0.37;
//        int tT = 15;
//        double l0 = new DryAirAmmount().calculate();
//        double hP = new AirSteamHeatContent(tempOne).calculate();
//        String fuelType = DRY_NATURE_GAS;
//        double d0 = dZero;
//        double cTn = 0.24;
//        int t1 = 130;
//        double h0 = new AtmosphereAirHeatContent(tempZero).calculate();
//
//        double alpha = new ExtraAirCoefficient(
//                qRn, efficiency, cT, tT, hP, l0, d0, cTn, t1, h0
//        ).calculate();
//        /*
//        (double alpha,
//                                    double lZero,
//                                    double dZero,
//                                    String fuelType)
//         */
//        Formulas form = new WetContainingAtEntrance(alpha, l0, d0);
//
//        System.out.println(form.calculate());
    }

    @Test
    public void commonCoefficientHeatTransferTest() throws InterruptedException {
		Thread.sleep(new Random().nextInt(3000) + 1000);

		assertTrue(true);
        /*
//        			double alphaOne,
//			double f,
//			double lambda,
//			double alphaTwo
//         */
//        double alphaOne = 6.02;
//        double f = 0.0025;
//        double lambda = 50;
//        double alphaTwo = 5.34;
//
//        Formulas form = new CommonCoefficientHeatTransfer(alphaOne, f, lambda, alphaTwo);
//
//        System.out.println(form.calculate());
    }

    @Test
    public void steamedWetAmmountPerHourTest() throws InterruptedException {
		Thread.sleep(new Random().nextInt(3000) + 1000);

		assertTrue(true);
//        double gH = 8000; //kgPh
//        double wK = 14; //percent
//        double wN = 21; //percent
//
//        Formulas form = new SteamedWetAmmountPerHour(gH, wK, wN);
//
//        assertEquals(651, (int)Math.floor(form.calculate()));
    }

    @Test
    public void heatLostPerKgWetTest() throws InterruptedException {
		Thread.sleep(new Random().nextInt(3000) + 1000);

		assertTrue(true);
//        String fuelType = DRY_NATURE_GAS;
//        int qRn = 8050;
//        double efficiency = 0.95;
//        double gH = 8000; //kgPh
//        double wK = 14; //percent
//        double wN = 21; //percent
//        double tAdd = 52;
//        double F = 47.23;
//        double alphaOne = 6.02;
//        double f = 0.0025;
//        double lambda = 50;
//        double alphaTwo = 5.34;
//        double cWet = 1;
//        double cZ = 0.37;
//        double cT = 0.37;
//        int tT = 15;
//        double cTn = 0.24;
//        double d0 = dZero;
//
//        double tAvg = new AverageHeatBearerTemp(tempOne, tAdd).calculate();
///*
//                                        double Gh,
//										double cZcomma,
//										double tAdd,
//										double tZero,
//										double Wh
// */
//        double k = new CommonCoefficientHeatTransfer(alphaOne, f, lambda, alphaTwo).calculate();
//        double wH = new SteamedWetAmmountPerHour(gH, wK, wN).calculate();
//        double qOs = new HeatLostPerKgWet(wH, F, k, tAvg, tempZero).calculate();
//        double cZcomma = new SandHeatCapacityAtOutput(wK, cZ, cWet).calculate();
//        double qZ = new HeatLossWithHotSandPerKgWet(gH, cZcomma, tAdd, tempZero, wH).calculate();
//        double h1 = new AtmosphereAirHeatContent(tempOne).calculate();
//        double sigma = new IncomeLostDiffsAtOutput(cWet, tempOne, qOs, qZ).calculate();
//        double hP = new AirSteamHeatContent(tempOne).calculate();
//        double l0 = new DryAirAmmount().calculate();
//        double h0 = new AtmosphereAirHeatContent(tempZero).calculate();
//        double alpha = new ExtraAirCoefficient(
//                qRn, efficiency, cT, tT, hP, l0, d0, cTn, tempOne, h0
//        ).calculate();
//        double d1 = new WetContainingAtEntrance(alpha, l0, d0).calculate();
//        double d2 = new HeatBearerWetContentAtOutput(CSV, tAdd, h1, sigma, d1).calculate();
//        double g = new HeatBearerConsumptionPerOneKGSteam(d2, d1).calculate();
//        double qB = new UnitHeatConsumptionToSteamingOneKGWet(g, h1, h0).calculate();
//        double hCondUnitToDry = new UnitConditionalFuelConsumptionToSandDrying(qB, wN, wK, efficiency).calculate();
//        double hNatUnitToDry = new UnitNaturalFuelConsumptionToSandDrying(hCondUnitToDry, qRn).calculate();
//        double qSteam = new HeatConsumptionToSteamHourly(wH, qB).calculate();
//        double qEnv = new HeatConsumptionToSteamHourly(wH, qOs).calculate();
//        double qHotSand = new HeatConsumptionToSteamHourly(wH, qZ).calculate();
//        double powerDryingMachine = new PowerDryingMachineDefiner(gH, hCondUnitToDry, cT, qRn).calculate();
//
//        System.out.println(powerDryingMachine);
    }

	@Test
	public void tests() throws InterruptedException {
		Thread.sleep(new Random().nextInt(3000) + 1000);

		assertTrue(true);
//		Configuration cfg = Configuration.getInstance();
//
//		cfg.setMaxTempHeatSand(52.0);
//		cfg.setLambda(50.0);
//		cfg.setTempHeatbearerAtEntrance(130.0);
//		cfg.setLowestHeatFuelAbility(8050.0);
//		cfg.setSandHumidityBeforeDrying(21.0);
//		cfg.setSandHumidityAfterDrying(14.0);
//		cfg.setFuelType("Природный газ", System.out::println);
//		cfg.setZeroHumidity("70", System.out::println);
//		cfg.setZeroTemp("15", System.out::println);
//		cfg.setDryerParams("Сушилка барабанная стационарная СЗСБ-8,0А", System.out::println);
//		Calculator calc = new Calculator();
//
//		System.out.println(String.format("hzero %.2f", calc.gethZero()));
//		System.out.println(String.format("lzero %.2f", calc.getlZero()));
//
//		System.out.println(String.format("hp %.2f", calc.gethP()));
//		System.out.println(String.format("alpha %.2f", calc.getAlpha()));
//		System.out.println(String.format("d1 %.2f", calc.getdOne()));
//		System.out.println(String.format("K %.2f", calc.getkBig()));
//		System.out.println(String.format("Wph %.2f", calc.getwPerHour()));
//		System.out.println(String.format("tavg %.2f", calc.gettAvg()));
//		System.out.println(String.format("qos %.2f", calc.getqOs()));
//		System.out.println(String.format("Czcomma %.2f", calc.getCZComma()));
//		System.out.println(String.format("qz %.2f", calc.getqZ()));
//		System.out.println(String.format("sigma %.2f", calc.getSigma()));
//		System.out.println(String.format("hone %.2f", calc.getHOne()));
//		System.out.println(String.format("d2 %.2f", calc.getdTwo()));
//		System.out.println(String.format("g %.2f", calc.getG()));
//		System.out.println(String.format("qv %.2f", calc.getqV()));
//		System.out.println(String.format("nusz %.2f", calc.getnUSz()));
//		System.out.println(String.format("nnsz %.2f", calc.getnNSz()));
//		System.out.println(String.format("Qisp %.2f", calc.getqBigIsp()));
//		System.out.println(String.format("Qbigos %.2f", calc.getqBigOs()));
//		System.out.println(String.format("qz %.2f", calc.getqZ()));
//		System.out.println(String.format("Q %.2f", calc.getqBig()));
	}
}
