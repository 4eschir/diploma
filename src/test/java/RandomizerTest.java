import by.gstu.diploma.drying.services.Randomizer;
import by.gstu.diploma.drying.services.RandomizerBuilder;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Random;

import static junit.framework.TestCase.assertTrue;

@Ignore
public class RandomizerTest {

	@Test
	public void randomizerTestWithDefault() {
		RandomizerBuilder builder = RandomizerBuilder.getBuilder();

		Randomizer rand = builder.build();
		assertTrue(true);
//		for(int i = 0; i <= 10; i ++)
//			System.out.println(rand.getNextRandom());
	}

	@Test
	public void randomizerTestWithoutAbnormal() {
		RandomizerBuilder builder = RandomizerBuilder.getBuilder();

		builder.withAbnormality(false);
		builder.withMaxStandardHum(10);
		assertTrue(true);
		Randomizer rand = builder.build();

//		for(int i = 0; i <= 10; i ++)
//			System.out.println(rand.getNextRandom());
	}

	@Test
	public void randomizerTestWithAbnormal50Percent() {
		assertTrue(true);

//		RandomizerBuilder builder = RandomizerBuilder.getBuilder();
//
//		builder.withAbnormalHum(100);
//		builder.withAbnormalProbability(50);
//		builder.withMaxStandardHum(10);
//
//		Randomizer rand = builder.build();
//
//		for(int i = 0; i <= 10; i ++)
//			System.out.println(rand.getNextRandom());
	}

	@Test
	public void gaussianTests() {
		double from = 14;
		double to = 15;
		boolean isTrap = false;
		double actual = 0;
		int counter = 0;

		while(!isTrap) {
			counter ++;
			double nextGauss = new Random().nextGaussian() * 100;
			if(isTrap = nextGauss >= from && nextGauss <= to)
				actual = nextGauss;
		}

		System.out.println(actual + " " + counter);
	}

}
