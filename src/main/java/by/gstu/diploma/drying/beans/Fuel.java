package by.gstu.diploma.drying.beans;

import java.util.Map;

import static by.gstu.diploma.drying.constants.FuelParameters.*;

public class Fuel {

	private final String fuelName;
	private Double ch4;
	private Double c2h6;
	private Double c3h8;
	private Double c4h10;
	private Double c5h12;
	private Double n2;
	private Double co2;

	public Fuel (String fuelName) {
	     this.fuelName = fuelName;
	}

	public void mapParameters (Map<String, Object> fuelMap) {
		this.ch4 = parseDouble(fuelMap.get(CH4));
		this.c2h6 = parseDouble(fuelMap.get(C2H6));
		this.c3h8 = parseDouble(fuelMap.get(C3H8));
		this.c4h10 = parseDouble(fuelMap.get(C4H10));
		this.c5h12 = parseDouble(fuelMap.get(C5H12));
		this.n2 = parseDouble(fuelMap.get(N2));
		this.co2 = parseDouble(fuelMap.get(CO2));
	};

	public Double getCh4() {
		return ch4;
	}

	public void setCh4(Double ch4) {
		this.ch4 = ch4;
	}

	public Double getC3h8() {
		return c3h8;
	}

	public void setC3h8(Double c3h8) {
		this.c3h8 = c3h8;
	}

	public Double getC4h10() {
		return c4h10;
	}

	public void setC4h10(Double c4h10) {
		this.c4h10 = c4h10;
	}

	public Double getC5h12() {
		return c5h12;
	}

	public void setC5h12(Double c5h12) {
		this.c5h12 = c5h12;
	}

	public Double getN2() {
		return n2;
	}

	public void setN2(Double n2) {
		this.n2 = n2;
	}

	public Double getCo2() {
		return co2;
	}

	public void setCo2(Double co2) {
		this.co2 = co2;
	}

	public Double getC2h6() {
		return c2h6;
	}

	public void setC2h6(Double c2h6) {
		this.c2h6 = c2h6;
	}

	private Double parseDouble(Object param) {
		return
				Double.valueOf(param.toString());
	}

	public String getFuelName() {
		return fuelName;
	}
}
