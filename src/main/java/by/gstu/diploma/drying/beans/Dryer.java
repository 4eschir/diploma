package by.gstu.diploma.drying.beans;

import java.util.Map;

public class Dryer {

	private final String dryerName;

	private Double dryerPerformance;
	private Double square;
	private Double shellThickness;

	public Dryer (String dryerName) {
		this.dryerName = dryerName;
	}

	public String getDryerName() {
		return dryerName;
	}

	public Double getDryerPerformance() {
		return dryerPerformance;
	}

	public Double getSquare() {
		return square;
	}

	public Double getShellThickness() {
		return shellThickness;
	}

	public void mapDryerParams(Map<String, Object> dryerMap) {
		this.dryerPerformance = parseDouble(dryerMap.get("performance"));
		this.square = parseDouble(dryerMap.get("square"));
		this.shellThickness = parseDouble(dryerMap.get("shellThickness"));
	}

	private Double parseDouble(Object param) {
		return
				Double.valueOf(param.toString());
	}
}
