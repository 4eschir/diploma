/**
 * 'next.fxml' Controller Class
 */

package by.gstu.diploma.drying.ui.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import by.gstu.diploma.drying.services.Calculator;
import by.gstu.diploma.drying.services.Configuration;
import by.gstu.diploma.drying.services.DynamicCalculator;
import by.gstu.diploma.drying.services.RandomizerBuilder;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import com.gluonhq.charm.glisten.control.TextField;
import javafx.scene.input.MouseEvent;

public class NextController {

	Calculator calc;
	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	@FXML // fx:id="h_zero"
	private Label h_zero; // Value injected by FXMLLoader

	@FXML // fx:id="l_zero"
	private Label l_zero; // Value injected by FXMLLoader

	@FXML // fx:id="h_p"
	private Label h_p; // Value injected by FXMLLoader

	@FXML // fx:id="alpha"
	private Label alpha; // Value injected by FXMLLoader

	@FXML // fx:id="d_one"
	private Label d_one; // Value injected by FXMLLoader

	@FXML // fx:id="k_big"
	private Label k_big; // Value injected by FXMLLoader

	@FXML // fx:id="w_per_hour"
	private Label w_per_hour; // Value injected by FXMLLoader

	@FXML // fx:id="q_os"
	private Label q_os; // Value injected by FXMLLoader

	@FXML // fx:id="temp_average"
	private Label temp_average; // Value injected by FXMLLoader

	@FXML // fx:id="q_z"
	private Label q_z; // Value injected by FXMLLoader

	@FXML // fx:id="sigma"
	private Label sigma; // Value injected by FXMLLoader

	@FXML // fx:id="d_two"
	private Label d_two; // Value injected by FXMLLoader

	@FXML // fx:id="g"
	private Label g; // Value injected by FXMLLoader

	@FXML // fx:id="g_v"
	private Label g_v; // Value injected by FXMLLoader

	@FXML // fx:id="n_u_sz"
	private Label n_u_sz; // Value injected by FXMLLoader

	@FXML // fx:id="n_n_sz"
	private Label n_n_sz; // Value injected by FXMLLoader

	@FXML // fx:id="q_big_isp"
	private Label q_big_isp; // Value injected by FXMLLoader

	@FXML // fx:id="q_big_os"
	private Label q_big_os; // Value injected by FXMLLoader

	@FXML // fx:id="q_big_z"
	private Label q_big_z; // Value injected by FXMLLoader

	@FXML // fx:id="q_big"
	private Label q_big; // Value injected by FXMLLoader

	@FXML // fx:id="fact_sand_humidity"
	private Label fact_sand_humidity; // Value injected by FXMLLoader

	@FXML
	private Button run_button;

	@FXML
	private Button stop_button;

	@FXML // fx:id="time_rate"
	private ChoiceBox<Integer> time_rate; // Value injected by FXMLLoader

	@FXML // fx:id="vibrations_max_humidity"
	private TextField vibrations_max_humidity; // Value injected by FXMLLoader

	@FXML // fx:id="abnormal_humidity"
	private TextField abnormal_humidity; // Value injected by FXMLLoader

	@FXML // fx:id="abnormal_switcher"
	private CheckBox abnormal_switcher; // Value injected by FXMLLoader

	@FXML // fx:id="probability_abnormal"
	private TextField probability_abnormal; // Value injected by FXMLLoader

	@FXML
	private Label total_ready;

	@FXML
	private Label fact_performance;

	@FXML
	private Label avg_performance;

	@FXML
	private LineChart<String, Double> perf_vs_ammount_chart;

	@FXML
	private LineChart<String, Double> hum_vs_ammount_chart;

	private RandomizerBuilder randBuilder;
	private DynamicCalculator dyCalc;
	private boolean isWorking = false;
	private XYChart.Series<String, Double> perfVsTotalProcessedChartValues;
	private XYChart.Series<String, Double> avgVsTotalProcessedChartValues;

	private XYChart.Series<String, Double> inputHumfVsTotalProcessedChartValues;
	private XYChart.Series<String, Double> finalHumVsTotalProcessedChartValues;

	Configuration cfg = Configuration.getInstance();

	@FXML
	void onAbnormalVibrationsSwitched(MouseEvent event) {
		randBuilder.withAbnormality(abnormal_switcher.isSelected());
		probability_abnormal.setDisable(abnormal_switcher.isSelected());
		abnormal_humidity.setDisable(abnormal_switcher.isSelected());
	}

	@FXML
	void onStartModeling(MouseEvent event) {
//		run_button.setDisable(true);
//		stop_button.setDisable(false);
		switchInputs();

//		abnormal_humidity.setDisable(true);
//		abnormal_switcher.setDisable(true);
//		probability_abnormal.setDisable(true);
//		vibrations_max_humidity.setDisable(true);

		this.isWorking = true;
		this.updateNumbers();
		this.runRandomizerJob();
	}

	private void switchInputs() {
		abnormal_switcher.setDisable(!abnormal_switcher.isDisable());
		run_button.setDisable(!run_button.isDisable());
		stop_button.setDisable(!stop_button.isDisable());
		abnormal_humidity.setDisable(!abnormal_humidity.isDisable());
		abnormal_switcher.setDisable(!abnormal_switcher.isDisable());
		probability_abnormal.setDisable(!probability_abnormal.isDisable());
		vibrations_max_humidity.setDisable(!vibrations_max_humidity.isDisable());
	}

	@FXML
	void onStopClicked(MouseEvent event) {
		switchInputs();

		this.isWorking = false;
	}

	private void initCharts() {
		perfVsTotalProcessedChartValues = new XYChart.Series<>();
		avgVsTotalProcessedChartValues = new XYChart.Series<>();

		inputHumfVsTotalProcessedChartValues = new XYChart.Series<>();
		finalHumVsTotalProcessedChartValues = new XYChart.Series<>();

		perfVsTotalProcessedChartValues.setName("Текущая производительность сушилки");
		avgVsTotalProcessedChartValues.setName("Средняя производительность сушилки");

		inputHumfVsTotalProcessedChartValues.setName("Актуальная влажность песка на входе");
		finalHumVsTotalProcessedChartValues.setName("Актуальная влажность песка на выходе");

		perf_vs_ammount_chart.getData().add(perfVsTotalProcessedChartValues);
		perf_vs_ammount_chart.getData().add(avgVsTotalProcessedChartValues);
		perf_vs_ammount_chart.setAnimated(false);

		hum_vs_ammount_chart.getData().add(inputHumfVsTotalProcessedChartValues);
		hum_vs_ammount_chart.getData().add(finalHumVsTotalProcessedChartValues);
		hum_vs_ammount_chart.setAnimated(false);
	};

	private void runRandomizerJob() {
		dyCalc = DynamicCalculator.getInstance().setRandomizer(randBuilder.build());

		Thread randomizer = new Thread(() -> {
			while(isWorking) {
				try {
					Thread.sleep(this.time_rate.getValue() * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				dyCalc.updateAll(this.time_rate.getValue());

				updateFactNumbers();
			}
		});

		randomizer.setDaemon(true);
		randomizer.start();
	}

	private void updateFactNumbers() {

		Platform.runLater(() -> {
			fact_sand_humidity.setText(String.format("%.2f", dyCalc.getCurrentFinalHumidity()));
			fact_performance.setText(String.format("%.2f", dyCalc.getCurrentDryerPerformance()));
			total_ready.setText(String.format("%.2f", dyCalc.getTotalSandProcessed()));
			avg_performance.setText(String.format("%.0f", dyCalc.getAveragePerformance()));

			updateChartValues();
		});
	}

	private void updateChartValues() {
		String totalSandAmmount = String.format("%.2f", dyCalc.getTotalSandProcessed());
		removeRedundant();
		perfVsTotalProcessedChartValues.getData()
				.add(
						new XYChart.Data<>(totalSandAmmount, dyCalc.getCurrentDryerPerformance())
				);
		avgVsTotalProcessedChartValues.getData()
				.add(new XYChart.Data<>(totalSandAmmount, dyCalc.getAveragePerformance()));

		inputHumfVsTotalProcessedChartValues.getData()
				.add(new XYChart.Data<>(totalSandAmmount, dyCalc.getCurrentStartingHumidity()));

		finalHumVsTotalProcessedChartValues.getData()
				.add(new XYChart.Data<>(totalSandAmmount, dyCalc.getCurrentFinalHumidity()));
	}

	private void removeRedundant() {
		if(perfVsTotalProcessedChartValues.getData().size() > 15)
			perfVsTotalProcessedChartValues.getData().remove(0);

		if(avgVsTotalProcessedChartValues.getData().size() > 15)
			avgVsTotalProcessedChartValues.getData().remove(0);

		if(inputHumfVsTotalProcessedChartValues.getData().size() > 15)
			inputHumfVsTotalProcessedChartValues.getData().remove(0);

		if(finalHumVsTotalProcessedChartValues.getData().size() > 15)
			finalHumVsTotalProcessedChartValues.getData().remove(0);
	}

	private void updateNumbers() {
		h_zero.setText(String.format("%.2f", calc.gethZero()));
		l_zero.setText(String.format("%.2f", calc.getlZero()));
		h_p.setText(String.format("%.2f", calc.gethP()));
		alpha.setText(String.format("%.2f", calc.getAlpha()));
		d_one.setText(String.format("%.2f", calc.getdOne()));
		k_big.setText(String.format("%.2f", calc.getkBig()));
		w_per_hour.setText(String.format("%.2f", calc.getwPerHour()));
		q_os.setText(String.format("%.2f", calc.getqOs()));
		temp_average.setText(String.format("%.2f", calc.gettAvg()));
		q_z.setText(String.format("%.2f", calc.getqZ()));
		sigma.setText(String.format("%.2f", calc.getSigma()));
		d_two.setText(String.format("%.2f", calc.getdTwo()));
		g.setText(String.format("%.2f", calc.getG()));
		g_v.setText(String.format("%.2f", calc.getqV()));
		n_u_sz.setText(String.format("%.2f", calc.getnUSz()));
		n_n_sz.setText(String.format("%.2f", calc.getnNSz()));
		q_big_isp.setText(String.format("%.2f", calc.getqBigIsp()));
		q_big_os.setText(String.format("%.2f", calc.getqBigOs()));
		q_big_z.setText(String.format("%.2f", calc.getqZ()));
		q_big.setText(String.format("%.2f", calc.getqBig()));
	}

	@FXML // This method is called by the FXMLLoader when initialization is complete
	void initialize() {
		stop_button.setDisable(true);
		this.randBuilder = RandomizerBuilder.getBuilder();

		this.validateMarkup();
		this.initTimeRateChoise();
		this.initVibrationsMaxHumidity();
		this.initAbnormalHumidity();
		this.initProbabilityAbnormal();
		this.initCharts();
		calc = new Calculator();
	}

	private void initVibrationsMaxHumidity() {
		vibrations_max_humidity.setText(String.valueOf(3));
		vibrations_max_humidity.textProperty().addListener((obs, oldVal, newVal) -> {
			if(isNumeric(newVal))
				randBuilder.withMaxStandardHum(Integer.parseInt(newVal));
			run_button.setDisable(!isNumeric(newVal));
		});
	}

	private void initAbnormalHumidity() {
		abnormal_humidity.setText(String.valueOf(83));
		abnormal_humidity.textProperty().addListener((obs, oldVal, newVal) -> {
			if(isNumeric(newVal))
				randBuilder.withAbnormalHum(Integer.parseInt(newVal));
			run_button.setDisable(!isNumeric(newVal));
		});
	}

	private void initProbabilityAbnormal() {
		probability_abnormal.setText(String.valueOf(3));
		probability_abnormal.textProperty().addListener((obs, oldVal, newVal) -> {
			if(isNumeric(newVal))
				randBuilder.withAbnormalProbability(Integer.parseInt(newVal));
			run_button.setDisable(!isNumeric(newVal));
		});
	}

	private void initTimeRateChoise() {
		ObservableList<Integer> timeRates = FXCollections.observableArrayList(
				1,
				2,
				3,
				4,
				5
		);
		time_rate.setItems(timeRates);
		time_rate.setValue(1);
	}

	private boolean isNumeric(String text) {
		return
				text.length() > 0 && text.matches("^[0-9]*$");
	}

	void validateMarkup() {
		assert h_zero != null : "fx:id=\"h_zero\" was not injected: check your FXML file 'next.fxml'.";
		assert l_zero != null : "fx:id=\"l_zero\" was not injected: check your FXML file 'next.fxml'.";
		assert h_p != null : "fx:id=\"h_p\" was not injected: check your FXML file 'next.fxml'.";
		assert alpha != null : "fx:id=\"alpha\" was not injected: check your FXML file 'next.fxml'.";
		assert d_one != null : "fx:id=\"d_one\" was not injected: check your FXML file 'next.fxml'.";
		assert k_big != null : "fx:id=\"k_big\" was not injected: check your FXML file 'next.fxml'.";
		assert w_per_hour != null : "fx:id=\"w_per_hour\" was not injected: check your FXML file 'next.fxml'.";
		assert q_os != null : "fx:id=\"q_os\" was not injected: check your FXML file 'next.fxml'.";
		assert temp_average != null : "fx:id=\"temp_average\" was not injected: check your FXML file 'next.fxml'.";
		assert q_z != null : "fx:id=\"q_z\" was not injected: check your FXML file 'next.fxml'.";
		assert sigma != null : "fx:id=\"sigma\" was not injected: check your FXML file 'next.fxml'.";
		assert d_two != null : "fx:id=\"d_two\" was not injected: check your FXML file 'next.fxml'.";
		assert g != null : "fx:id=\"g\" was not injected: check your FXML file 'next.fxml'.";
		assert g_v != null : "fx:id=\"g_v\" was not injected: check your FXML file 'next.fxml'.";
		assert n_u_sz != null : "fx:id=\"n_u_sz\" was not injected: check your FXML file 'next.fxml'.";
		assert n_n_sz != null : "fx:id=\"n_n_sz\" was not injected: check your FXML file 'next.fxml'.";
		assert q_big_isp != null : "fx:id=\"q_big_isp\" was not injected: check your FXML file 'next.fxml'.";
		assert q_big_os != null : "fx:id=\"q_big_os\" was not injected: check your FXML file 'next.fxml'.";
		assert q_big_z != null : "fx:id=\"q_big_z\" was not injected: check your FXML file 'next.fxml'.";
		assert q_big != null : "fx:id=\"q_big\" was not injected: check your FXML file 'next.fxml'.";
		assert fact_sand_humidity != null : "fx:id=\"fact_sand_humidity\" was not injected: check your FXML file 'next.fxml'.";
		assert run_button != null : "fx:id=\"run_button\" was not injected: check your FXML file 'next.fxml'.";
		assert time_rate != null : "fx:id=\"time_rate\" was not injected: check your FXML file 'next.fxml'.";
		assert vibrations_max_humidity != null : "fx:id=\"vibrations_max_humidity\" was not injected: check your FXML file 'next.fxml'.";
		assert abnormal_switcher != null : "fx:id=\"abnormal_switcher\" was not injected: check your FXML file 'next.fxml'.";
		assert abnormal_humidity != null : "fx:id=\"abnormal_humidity\" was not injected: check your FXML file 'next.fxml'.";
		assert probability_abnormal != null : "fx:id=\"probability_abnormal\" was not injected: check your FXML file 'next.fxml'.";
	}

}
