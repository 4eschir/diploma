package by.gstu.diploma.drying.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

	public static void main(String[]args){
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("markup.fxml"));
		primaryStage.getIcons().add(new Image(String.valueOf(getClass().getClassLoader().getResource("hourglass.png"))));
		primaryStage.setTitle("Sand Drying Modeling");
		primaryStage.setScene(new Scene(root, 1560, 1024));
		primaryStage.show();
	}
}
