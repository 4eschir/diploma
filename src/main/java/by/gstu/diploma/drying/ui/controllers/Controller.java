package by.gstu.diploma.drying.ui.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import by.gstu.diploma.drying.beans.Dryer;
import by.gstu.diploma.drying.services.Configuration;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import com.gluonhq.charm.glisten.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import static by.gstu.diploma.drying.constants.FuelParameters.*;

public class Controller {

	private Configuration cfg;

	public Controller () {
	    cfg = Configuration.getInstance();
	}

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private ChoiceBox<String> fuel_type_picker;

	@FXML
	private ChoiceBox<String> dryer_type_picker;

	@FXML
	private ChoiceBox<Integer> temp_zero_picker;

	@FXML
	private ChoiceBox<Integer> humidity_zero_picker;

	@FXML
	private TextField sand_humidity_before_drying;

	@FXML
	private TextField sand_humidity_after_drying;

	@FXML // fx:id="lowest_heat_fuel_ability"
	private TextField lowest_heat_fuel_ability; // Value injected by FXMLLoader

	@FXML // fx:id="lambda"
	private TextField lambda; // Value injected by FXMLLoader

	@FXML // fx:id="temp_heatbearer_at_entrance"
	private TextField temp_heatbearer_at_entrance; // Value injected by FXMLLoader

	@FXML // fx:id="max_temp_heat_sand"
	private TextField max_temp_heat_sand; // Value injected by FXMLLoader

	@FXML
	private Label square_dryer;

	@FXML
	private Label thickness_dryer;

	@FXML
	private Label ch4;

	@FXML
	private Label c2h6;

	@FXML
	private Label c3h8;

	@FXML
	private Label c4h10;

	@FXML
	private Label c5h12;

	@FXML
	private Label n2;

	@FXML
	private Label co2;


	@FXML
	private Label performance_dryer;

	@FXML
	private Label air_humidity;

	@FXML
	private Button forward_button;

	@FXML
	void initialize() {
		setDefault();
		validateMarkup();

		forward_button.setDisable(true);

		composeFuelTypeChoiseBox();
		composeDryerTypePicker();
		composeTempZeroPicker();
		composeHumidityZeroPicker();

		initSandHB4Drying();
		initSandHAfterDrying();
		initLowestHeatFuelAbility();
		initLambda();
		initTempHeatbearerAtEntrance();
		initMaxTempHeatSand();
		validateFields();
	}

	private void setDefault() {
		cfg.setMaxTempHeatSand(52.0);
		cfg.setLambda(50.0);
		cfg.setTempHeatbearerAtEntrance(130.0);
		cfg.setLowestHeatFuelAbility(8050.0);
		cfg.setSandHumidityBeforeDrying(21.0);
		cfg.setSandHumidityAfterDrying(14.0);
		cfg.setFuelType("Природный газ", map -> {
			ch4.setText(map.get(CH4).toString());
			c2h6.setText(map.get(C2H6).toString());
			c3h8.setText(map.get(C3H8).toString());
			c4h10.setText(map.get(C4H10).toString());
			c5h12.setText(map.get(C5H12).toString());
			n2.setText(map.get(N2).toString());
			co2.setText(map.get(CO2).toString());
		});
		cfg.setZeroHumidity("70", value -> air_humidity.setText(value));
		cfg.setZeroTemp("15", value -> air_humidity.setText(value));
		cfg.setDryerParams("C-50", map -> {
			square_dryer.setText(map.get("square").toString());
			thickness_dryer.setText(map.get("shellThickness").toString());
			performance_dryer.setText(map.get("performance").toString());
		});
	}

	@FXML
	void button_next_clicked(MouseEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getClassLoader().getResource("next.fxml"));
			loader.load();
			Parent parent = loader.getRoot();
			Stage stage = new Stage();
			stage.setTitle("Sand Drying Modeling");
			forward_button.getScene().getWindow().hide();
			stage.setScene(new Scene(parent));
			stage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void composeHumidityZeroPicker() {
		ObservableList<Integer> humsZero = FXCollections.observableArrayList(
				100,
				90,
				80,
				70,
				60,
				50,
				40,
				30
		);
		humidity_zero_picker.setItems(humsZero);

		humidity_zero_picker.setValue(cfg.getZeroHumidity() == null ? Integer.valueOf("") : (int)cfg.getZeroHumidity());

		humidity_zero_picker.getSelectionModel()
				.selectedItemProperty()
				.addListener((a, b, c) -> {
					cfg.setZeroHumidity(a.getValue().toString(), value -> air_humidity.setText(value));
					validateFields();
				});
	}

	private void composeTempZeroPicker() {

		ObservableList<Integer> tempsZero = FXCollections.observableArrayList(
				-15,
				-10,
				-5,
				0,
				5,
				10,
				15,
				20,
				25
		);
		temp_zero_picker.setItems(tempsZero);

		temp_zero_picker.setValue(cfg.getZeroTemp() == null ? Integer.valueOf("") : cfg.getZeroTemp());

		temp_zero_picker.getSelectionModel()
				.selectedItemProperty()
				.addListener((a, b, c) -> {
					cfg.setZeroTemp(a.getValue().toString(), value -> air_humidity.setText(value));
					validateFields();
				});
	}

	private void initMaxTempHeatSand() {
		max_temp_heat_sand.setText(cfg.getMaxTempHeatSand() == null ? "" : String.valueOf(cfg.getMaxTempHeatSand()));
		max_temp_heat_sand.textProperty().addListener((obs, oldVal, newVal) -> {
			if(isNumeric(newVal)) {
				cfg.setMaxTempHeatSand(parseDouble(newVal));
				validateFields();
			} else {
				cfg.setMaxTempHeatSand(null);
				validateFields();
			}
		});
	}

	private void initSandHAfterDrying() {
		sand_humidity_after_drying.setText(cfg.getSandHumidityAfterDrying() == null ? "" : String.valueOf(cfg.getSandHumidityAfterDrying()));

		sand_humidity_after_drying.textProperty().addListener((obs, oldVal, newVal) -> {
			if(isNumeric(newVal)) {
				cfg.setSandHumidityAfterDrying(parseDouble(newVal));
				validateFields();
			} else {
				cfg.setSandHumidityAfterDrying(null);
				validateFields();
			}
		});
	}

	private void initSandHB4Drying() {
		sand_humidity_before_drying
				.setText(
						cfg.getSandHumidityBeforeDrying() == null ? ""
								: String.valueOf(cfg.getSandHumidityBeforeDrying()));

		sand_humidity_before_drying.textProperty().addListener((obs, oldVal, newVal) -> {
			if(isNumeric(newVal)) {
				cfg.setSandHumidityBeforeDrying(parseDouble(newVal));
				validateFields();
			} else {
				cfg.setSandHumidityBeforeDrying(null);
				validateFields();
			}
		});
	}

	private void initLowestHeatFuelAbility() {
		lowest_heat_fuel_ability
				.setText(
						cfg.getLowestHeatFuelAbility() == null ? ""
								: String.valueOf(cfg.getLowestHeatFuelAbility()));

		lowest_heat_fuel_ability.textProperty().addListener((obs, oldVal, newVal) -> {
			if(isNumeric(newVal)) {
				cfg.setLowestHeatFuelAbility(parseDouble(newVal));
				validateFields();
			} else {
				cfg.setLowestHeatFuelAbility(null);
				validateFields();
			}
		});
	}

	private void initLambda() {
		lambda.setText(cfg.getLambda() == null ? "" : String.valueOf(cfg.getLambda()));

		lambda.textProperty().addListener((obs, oldVal, newVal) -> {
			if(isNumeric(newVal)) {
				cfg.setLambda(parseDouble(newVal));
				validateFields();
			} else {
				cfg.setLambda(null);
				validateFields();
			}
		});
	}

	private void initTempHeatbearerAtEntrance() {
		temp_heatbearer_at_entrance.setText(cfg.getTempHeatbearerAtEntrance() == null ? "" : String.valueOf(cfg.getTempHeatbearerAtEntrance()));
		temp_heatbearer_at_entrance.textProperty().addListener((obs, oldVal, newVal) -> {
			if(isNumeric(newVal)) {
				cfg.setTempHeatbearerAtEntrance(parseDouble(newVal));
				validateFields();
			} else {
				cfg.setTempHeatbearerAtEntrance(null);
				validateFields();
			}
		});
	}

	private void composeDryerTypePicker() {
		ObservableList<String> dryerTypes = FXCollections.observableArrayList(
				"C-0,3",
				"C-1",
				"C-2",
				"C-4",
				"C-5",
				"C-6",
				"C-7",
				"C-10",
				"C-20",
				"C-30",
				"C-40",
				"C-50",
				"C-60"
		);
		dryer_type_picker.setItems(dryerTypes);
		dryer_type_picker.setValue(cfg.getDryer() == null ? "" : cfg.getDryer().getDryerName());
		dryer_type_picker.getSelectionModel()
				.selectedItemProperty()
				.addListener((a, b, c) -> {
					cfg.setDryerParams(a.getValue().toString(), map -> {
						square_dryer.setText(map.get("square").toString());
						thickness_dryer.setText(map.get("shellThickness").toString());
						performance_dryer.setText(map.get("performanсe").toString());
					});
					validateFields();
				});
	}

	private void composeFuelTypeChoiseBox() {
		ObservableList<String> fuelTypes = FXCollections.observableArrayList("Природный газ");
		fuel_type_picker.setItems(fuelTypes);
		fuel_type_picker.setValue(cfg.getFuel() == null ? "" : cfg.getFuel().getFuelName());
		fuel_type_picker.getSelectionModel()
				.selectedItemProperty()
				.addListener((a, b, c) -> cfg.setFuelType(a.getValue().toString(), map -> {
					ch4.setText(map.get(CH4).toString());
					c2h6.setText(map.get(C2H6).toString());
					c3h8.setText(map.get(C3H8).toString());
					c4h10.setText(map.get(C4H10).toString());
					c5h12.setText(map.get(C5H12).toString());
					n2.setText(map.get(N2).toString());
					co2.setText(map.get(CO2).toString());
				}));
	}

	private Double parseDouble(String text) {
		return
				Double.valueOf(text);

	}

	private boolean isNumeric(String text) {
		return
				text.length() > 0 && text.matches("^[0-9]*\\.*[0-9]$");
	}

	@FXML // This method is called by the FXMLLoader when initialization is complete
	private void validateMarkup() {
		assert sand_humidity_before_drying != null : "fx:id=\"sand_humidity_before_drying\" was not injected: check your FXML file 'markup.fxml'.";
		assert sand_humidity_after_drying != null : "fx:id=\"sand_humidity_after_drying\" was not injected: check your FXML file 'markup.fxml'.";
		assert temp_zero_picker != null : "fx:id=\"temp_zero_picker\" was not injected: check your FXML file 'markup.fxml'.";
		assert humidity_zero_picker != null : "fx:id=\"humidity_zero_picker\" was not injected: check your FXML file 'markup.fxml'.";
		assert fuel_type_picker != null : "fx:id=\"fuel_type_picker\" was not injected: check your FXML file 'markup.fxml'.";
		assert lowest_heat_fuel_ability != null : "fx:id=\"lowest_heat_fuel_ability\" was not injected: check your FXML file 'markup.fxml'.";
		assert lambda != null : "fx:id=\"lambda\" was not injected: check your FXML file 'markup.fxml'.";
		assert temp_heatbearer_at_entrance != null : "fx:id=\"temp_heatbearer_at_entrance\" was not injected: check your FXML file 'markup.fxml'.";
		assert max_temp_heat_sand != null : "fx:id=\"max_temp_heat_sand\" was not injected: check your FXML file 'markup.fxml'.";
		assert dryer_type_picker != null : "fx:id=\"dryer_type_picker\" was not injected: check your FXML file 'markup.fxml'.";
		assert square_dryer != null : "fx:id=\"square_dryer\" was not injected: check your FXML file 'markup.fxml'.";
		assert thickness_dryer != null : "fx:id=\"thickness_dryer\" was not injected: check your FXML file 'markup.fxml'.";
		assert performance_dryer != null : "fx:id=\"performance_dryer\" was not injected: check your FXML file 'markup.fxml'.";
		assert ch4 != null : "fx:id=\"ch4\" was not injected: check your FXML file 'markup.fxml'.";
		assert c2h6 != null : "fx:id=\"c2h6\" was not injected: check your FXML file 'markup.fxml'.";
		assert c3h8 != null : "fx:id=\"c3h8\" was not injected: check your FXML file 'markup.fxml'.";
		assert c4h10 != null : "fx:id=\"c4h10\" was not injected: check your FXML file 'markup.fxml'.";
		assert c5h12 != null : "fx:id=\"c5h12\" was not injected: check your FXML file 'markup.fxml'.";
		assert n2 != null : "fx:id=\"n2\" was not injected: check your FXML file 'markup.fxml'.";
		assert co2 != null : "fx:id=\"co2\" was not injected: check your FXML file 'markup.fxml'.";
		assert forward_button != null : "fx:id=\"forward_button\" was not injected: check your FXML file 'markup.fxml'.";
		assert air_humidity != null : "fx:id=\"air_humidity\" was not injected: check your FXML file 'markup.fxml'.";
	}

	private void validateFields() {
		Dryer dryer = cfg.getDryer();
		Double sandHumidityAfterDrying = cfg.getSandHumidityAfterDrying();
		Integer zeroHumidity = cfg.getZeroHumidity();
		Integer zeroTemp = cfg.getZeroTemp();
		Double lowestHeatFuelAbility = cfg.getLowestHeatFuelAbility();
		Double lambda = cfg.getLambda();
		Double tempHeatbearerAtEntrance = cfg.getTempHeatbearerAtEntrance();
		Double maxTempHeatSand = cfg.getMaxTempHeatSand();

		boolean isDisabled =
						sandHumidityAfterDrying == null ||
						zeroHumidity == null ||
						zeroTemp == null ||
						dryer == null ||
						lowestHeatFuelAbility == null ||
						lambda == null ||
						tempHeatbearerAtEntrance == null ||
						maxTempHeatSand == null;

		forward_button.setDisable(isDisabled);

	}

}
