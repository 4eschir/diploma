package by.gstu.diploma.drying.formulas;

public interface Formulas {

    /*
    Cm
    Hn
    m
    n
     */

    //calculating L0, dry air amount to fully burn out 1kg fuel
    public double calculate();

}
