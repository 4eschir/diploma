package by.gstu.diploma.drying.formulas;

/*
cZcomma - see 1.9
*/
public class SandHeatCapacityAtOutput implements Formulas {

	double wK;
	double cZ;
	double cWet;

	public SandHeatCapacityAtOutput (double wK, double cZ, double cWet) {
	     this.wK = wK;
	     this.cZ = cZ;
	     this.cWet = cWet;
	}

	public double calculate() {
		return ((100 - wK) * cZ + wK * cWet)/100;
	}
}
