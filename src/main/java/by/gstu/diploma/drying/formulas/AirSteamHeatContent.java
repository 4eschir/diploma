package by.gstu.diploma.drying.formulas;

/*
hп, see 2,4
 */
public class AirSteamHeatContent implements Formulas {

    private final Double t1;

    public AirSteamHeatContent(Double t1) {
        this.t1 = t1;
    }

    public double calculate() {
        return 595 + 0.47 * t1;
    }
}
