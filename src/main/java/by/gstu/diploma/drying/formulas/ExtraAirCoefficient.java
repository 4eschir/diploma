package by.gstu.diploma.drying.formulas;

/*
Alpha, extra air coefficient for gas fuel,
see 2.3
 */

import by.gstu.diploma.drying.beans.Fuel;
import by.gstu.diploma.drying.services.Configuration;

import static by.gstu.diploma.drying.constants.ConstantValues.*;

public class ExtraAirCoefficient implements Formulas {

    private final double qRn;
    private final double hP;
    private final double l0;
    private final double d0;
    private final double t1;
    private final double h0;

    private Fuel fuel;

    public ExtraAirCoefficient(double qRn,
                               double hP,
                               double l0,
                               double d0,
                               double t1,
                               double h0) {
        this.qRn = qRn;
        this.hP = hP;
        this.fuel = Configuration.getInstance().getFuel();
        this.l0 = l0;
        this.d0 = d0;
        this.t1 = t1;
        this.h0 = h0;
    }

    public double calculate() {
        double top = divident();
        double bot = divider();
        return top/bot;
    }

    private double divident() {
        //noinspection PointlessArithmeticExpression
        double sum = calculateSum();
        return
                qRn * NJU_T + C_T * T_T - ((hP*9)/100) * sum;
    }

    private double calculateSum() {
        double ch4 = fuel.getCh4();
        double c2H6 = fuel.getC2h6();
        double c3H8 = fuel.getC3h8();
        double c4H10 = fuel.getC4h10();
        double c5H12 = fuel.getC5h12();

        return  (ch4 * 4) / (12*1 + 4) +
                (c2H6 * 6) / (12*2 + 6) +
                (c3H8 * 8) / (12*3 + 8) +
                (c4H10 * 10) / (12*4 + 10) +
                (c5H12 * 12) / (12*5 + 12);
    }
    private double divider() {
        return
                l0 * (d0 * hP * 0.001 + C_TN * t1 - h0);
    }
}
