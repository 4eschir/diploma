package by.gstu.diploma.drying.formulas;

import by.gstu.diploma.drying.beans.Fuel;
import by.gstu.diploma.drying.services.Configuration;

/*
L0 - see 2.2
 */
public class DryAirAmmount implements Formulas {

    private final Fuel fuel;

    public DryAirAmmount() {
        this.fuel = Configuration.getInstance().getFuel();
    }

    public double calculate() {
        //noinspection PointlessArithmeticExpression
        return
                1.38 * fuel.getCh4() *((1 + 0.25 * 4) / (12 * 1 + 4)) +
                1.38 * fuel.getC2h6() * ((2 + 0.25 * 6) / (12 * 2 + 6)) +
                1.38 * fuel.getC3h8() * ((3 + 0.25 * 8)/(12 * 3 + 8)) +
                1.38 * fuel.getC4h10() * ((4 + 0.25 * 10)/(12 * 4 + 10)) +
                1.38 * fuel.getC5h12() * ((5 + 0.25 * 12)/(12 * 5 + 12));
    }



}
