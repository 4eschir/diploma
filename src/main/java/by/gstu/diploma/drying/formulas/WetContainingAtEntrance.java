package by.gstu.diploma.drying.formulas;

import by.gstu.diploma.drying.beans.Fuel;
import by.gstu.diploma.drying.services.Configuration;

/*
d1, see 2.4
 */
public class WetContainingAtEntrance implements Formulas {

    private final double alpha;
    private final double lZero;
    private final double dZero;

    private Fuel fuel; //

    public WetContainingAtEntrance (double alpha,
                                    double lZero,
                                    double dZero) {
         this.alpha = alpha;
         this.lZero = lZero;
         this.dZero = dZero;
         fuel = Configuration.getInstance().getFuel();
    }

    public double calculate() {
        double top = divident();
        double bot = divider();

        return top/bot;
    }

    @SuppressWarnings("PointlessArithmeticExpression")
    private double divider() {
        double ch4 = fuel.getCh4();
        double c2H6 = fuel.getC2h6();
        double c3H8 = fuel.getC3h8();
        double c4H10 = fuel.getC4h10();
        double c5H12 = fuel.getC5h12();

        return
                1 -
                        0.09 * (
                                ((ch4 * 4)/(12 * 1 + 4)) +
                                ((c2H6 * 6)/(12 * 2 + 6)) +
                                ((c3H8 * 8)/(12 * 3 + 8)) +
                                ((c4H10 * 10)/(12 * 4 + 10)) +
                                ((c5H12 * 12)/(12 * 5 + 12))
                                ) +
                        (alpha *
                        lZero);

    }

    private double divident() {
        double ch4 = fuel.getCh4();
        double c2H6 = fuel.getC2h6();
        double c3H8 = fuel.getC3h8();
        double c4H10 = fuel.getC4h10();
        double c5H12 = fuel.getC5h12();

        //noinspection IntegerDivisionInFloatingPointContext,PointlessArithmeticExpression
        return
                (
                        ((ch4 * 90 * 4) / (12 * 1 + 4)) +
                        ((c2H6 * 90 * 6) / (12 * 2 + 6)) +
                        ((c3H8 * 90 * 8) / (12 * 3 + 8)) +
                        ((c4H10 * 90 * 10) / (12 * 4 + 10)) +
                        ((c5H12 * 90 * 12) / (12 * 5 + 12))
                )
                +
                        (alpha *
                        lZero *
                        dZero);


    }
}
