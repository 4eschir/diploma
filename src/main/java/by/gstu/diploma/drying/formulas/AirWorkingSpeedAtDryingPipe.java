package by.gstu.diploma.drying.formulas;

/*
WВ - air working speed at drying pipe
Wвс - sucking speed
dp - sand max quant diameter, 0.001 m
c - coefficient 5.7
TODO are we need it at all?
 */

@Deprecated
public class AirWorkingSpeedAtDryingPipe implements Formulas {

    private static final double dp = 0.001;
    private static final double c = 5.7;

    public double calculate() {
        return 0;
    }

    private double Wbc() {
        return 0;
    }
}
