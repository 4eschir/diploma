package by.gstu.diploma.drying.formulas;

/*
just Q, to define drying machine power, see 2.19
 */

public class PowerDryingMachineDefiner implements Formulas {

	private static final int SEVEN = 7;
	private static final double COEFF = 0.001;

	private final double gH;
	private final double hCondUnitToDry;
	private final double cT;
	private final double qRn;

	public PowerDryingMachineDefiner(double gH, double hCondUnitToDry, double cT, double qRn) {
		this.gH = gH;
		this.hCondUnitToDry = hCondUnitToDry;
		this.cT = cT;
		this.qRn = qRn;
	}

	public double calculate() {
		return SEVEN * gH*1000 * hCondUnitToDry * (COEFF + cT/qRn);
	}
}
