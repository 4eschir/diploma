package by.gstu.diploma.drying.formulas;
/*
d2,  see 2.11
 */
public class HeatBearerWetContentAtOutput implements Formulas {

	private final double Csv;
	private final double tAdd;
	private final double h1;
	private final double sigma;
	private final double d1;

	public HeatBearerWetContentAtOutput(double csv,
										double tAdd,
										double h1,
										double sigma,
										double d1) {
		this.Csv = csv;
		this.tAdd = tAdd;
		this.h1 = h1;
		this.sigma = sigma;
		this.d1 = d1;
	}

	public double calculate() {
		double top = dividend();
		double bot = divider();
		return top/bot;
	}

	private double dividend() {
		return
				1000 *
				(Csv *
				tAdd -
				h1) +
				sigma *
				d1;
	}

	private double divider() {
		return
				sigma -
				(595 +
				0.47 *
				tAdd);
	}
}
