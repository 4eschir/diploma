package by.gstu.diploma.drying.formulas;

import static by.gstu.diploma.drying.constants.ConstantValues.SEVEN_TH;

/*
Н н-сз see 2.15
 */
public class UnitNaturalFuelConsumptionToSandDrying implements Formulas {

	private final double hCondUnitToDry;
	private final double qRn;

	public UnitNaturalFuelConsumptionToSandDrying(double hCondUnitToDry, double qRn) {
		this.hCondUnitToDry = hCondUnitToDry;
		this.qRn = qRn;
	}

	public double calculate() {
		return hCondUnitToDry * (SEVEN_TH/qRn);
	}
}
