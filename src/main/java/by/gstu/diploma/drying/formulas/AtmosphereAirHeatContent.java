package by.gstu.diploma.drying.formulas;

import by.gstu.diploma.drying.services.Configuration;

import static by.gstu.diploma.drying.constants.ConstantValues.CSV;

/*calculating atmosphere air heatContent
* h0, See 2.1 formula in report
* h1, tempZero is not actual zero but one
* */
public class AtmosphereAirHeatContent implements Formulas {

    private final double tempZero;

    public AtmosphereAirHeatContent(double tempZero) {
        this.tempZero = tempZero;
    }

    public double calculate() {
        return
                CSV *
                tempZero +
                Configuration.getInstance().getDZero() *
                (595 + 0.47 * tempZero) * 0.001;
    }
}
