package by.gstu.diploma.drying.formulas;

/*
g, see 2.12
 */
public class HeatBearerConsumptionPerOneKGSteam implements Formulas {

	private final double d2;

	public HeatBearerConsumptionPerOneKGSteam(double d2, double d1) {
		this.d2 = d2;
		this.d1 = d1;
	}

	private final double d1;



	public double calculate() {
		return 1000 / (d2 - d1);
	}
}
