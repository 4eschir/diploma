package by.gstu.diploma.drying.formulas;

/*
Qisp, see 2.16 with qB, 2.17 if second parameter is qOs, 2.18 with qZ
 */

import static by.gstu.diploma.drying.constants.ConstantValues.COEFF;

public class HeatConsumptionToSteamHourly implements Formulas {

	private final double wH;
	private final double qB;

	public HeatConsumptionToSteamHourly(double wH, double qB) {
		this.wH = wH;
		this.qB = qB;
	}

	public double calculate() {
		return wH * qB * COEFF;
	}
}
