package by.gstu.diploma.drying.formulas;

/*
sigma, see 2.10
 */
public class IncomeLostDiffsAtOutput implements Formulas {

	private final double qOs;
	private final double cWet;
	private final double tOne;
	private final double qZ;

	public IncomeLostDiffsAtOutput (double cWet,
									double tOne,
									double qOs,
									double qZ) {
	     this.qOs = qOs;
	     this.cWet = cWet;
	     this.tOne = tOne;
	     this.qZ = qZ;
	}

	public double calculate() {
		return
				cWet * tOne - qOs - qZ;
	}
}
