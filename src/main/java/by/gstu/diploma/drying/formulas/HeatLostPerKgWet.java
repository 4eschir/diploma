package by.gstu.diploma.drying.formulas;

/*
qOc see 2.7

 */
public class HeatLostPerKgWet implements Formulas {

	private final double wH;
	private final double f;
	private final double k;
	private final double tAvg;
	private final double tZero;

	public HeatLostPerKgWet (double wH, double f, double k, double tAvg, double tZero) {
	     this.wH = wH;
	     this.f = f;
	     this.k = k;
	     this.tAvg = tAvg;
	     this.tZero = tZero;
	}

	public double calculate() {
		double top = divident();
		return top/ wH;
	}

	private double divident() {
		return
				f * k *(tAvg - tZero);
	}

}
