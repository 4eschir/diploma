package by.gstu.diploma.drying.formulas;

/*
Hu-sz - see 2.14
 */

public class UnitConditionalFuelConsumptionToSandDrying implements Formulas {

	private final double qB;
	private final double wH;
	private final double wK;
	private final double njuT;

	public UnitConditionalFuelConsumptionToSandDrying(double qB, double wH, double wK, double njuT) {
		this.qB = qB;
		this.wH = wH;
		this.wK = wK;
		this.njuT = njuT;
	}

	public double calculate() {
		double top = dividend();
		double bot = divider();

		return top/bot;
	}

	private double dividend() {
		return
				qB * (wH - wK);
	}

	private double divider() {
		return
				7 * njuT * (100 - wH);
	}
}
