package by.gstu.diploma.drying.formulas;

/*
qZ - see 2.9
 */

public class HeatLossWithHotSandPerKgWet implements Formulas {

	private final double Gh;
	private final double cZcomma;
	private final double tAdd;
	private final double tZero;
	private final double Wh;

	public HeatLossWithHotSandPerKgWet (double Gh,
										double cZcomma,
										double tAdd,
										double tZero,
										double Wh) {
	     this.Gh = Gh;
	     this.cZcomma = cZcomma;
	     this.tAdd = tAdd;
	     this.tZero = tZero;
	     this.Wh = Wh;
	}

	public double calculate() {
		return (Gh*1000 * cZcomma * (tAdd - tZero))/Wh;
	}
}
