package by.gstu.diploma.drying.formulas;

/*
qB - see 2.13
 */
public class UnitHeatConsumptionToSteamingOneKGWet implements Formulas {

	private final double g;
	private final double h1;
	private final double h0;

	public UnitHeatConsumptionToSteamingOneKGWet(double g, double h1, double h0) {
		this.g = g;
		this.h1 = h1;
		this.h0 = h0;
	}

	public double calculate() {
		return g * (h1 - h0);
	}
}
