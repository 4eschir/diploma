package by.gstu.diploma.drying.formulas;

/*
K, see 2.5,  kkall/(m2 * h * Celsius)
 */

import static by.gstu.diploma.drying.constants.ConstantValues.ALPHA_ONE;
import static by.gstu.diploma.drying.constants.ConstantValues.ALPHA_TWO;

public class CommonCoefficientHeatTransfer implements Formulas {

	private final double f;
	private final double lambda;

	public CommonCoefficientHeatTransfer (
			double f,
			double lambda
										  ) {
		this.f = f;
		this.lambda = lambda;
	}

	public double calculate() {
		return  Math.pow(
					(1/ALPHA_ONE) +
					(f/lambda) +
					(1/ALPHA_TWO),
				-1);
	}
}
