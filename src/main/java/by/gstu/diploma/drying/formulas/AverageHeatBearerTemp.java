package by.gstu.diploma.drying.formulas;

/*
tAverage - see 2.8
 */

public class AverageHeatBearerTemp implements Formulas {

	private final double tOne;
	private final double tAdd;

	public AverageHeatBearerTemp (double tOne, double tAdd) {
	     this.tOne = tOne;
	     this.tAdd = tAdd;
	}

	public double calculate() {
		return
				(tOne + tAdd)/2;
	}
}
