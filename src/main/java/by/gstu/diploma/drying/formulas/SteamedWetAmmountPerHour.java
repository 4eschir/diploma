package by.gstu.diploma.drying.formulas;

/*
Wh, see 2.6
 */

public class SteamedWetAmmountPerHour implements Formulas{

	private final double gH;
	private final double wK;
	private final double wH;

	public SteamedWetAmmountPerHour (
			double gH,
			double wK,
			double wH
	) {
		this.wK = wK;
		this.wH = wH;
	    this.gH = gH;
	}

	public double calculate() {
		double top = divident();
		double bot = divider();

		return gH * (top/bot)*1000;
	}

	private double divider() {
		return 100 - wK;
	}

	private double divident() {
		return wH - wK;
	}
}
