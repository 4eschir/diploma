package by.gstu.diploma.drying.constants;

public class FuelParameters {

        public static final String CH4  = "CH4";
        public static final String C2H6 = "C2H6";
        public static final String C3H8 = "C3H8";
        public static final String C4H10 = "C4H10";
        public static final String C5H12 = "C5H12";
        public static final String N2 = "N2";
        public static final String CO2 = "CO2";
}
