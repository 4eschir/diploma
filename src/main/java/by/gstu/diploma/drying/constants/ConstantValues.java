package by.gstu.diploma.drying.constants;

public class ConstantValues {

	public static final double CSV = 0.24;
	public static final double C_T = 0.37;
	public static final double C_TN = 0.24;
	public static final double NJU_T = 0.95;
	public static final int T_T = 15;
	public static final double ALPHA_ONE = 6.02;
	public static final double ALPHA_TWO = 5.34;
	public static final double C_Z = 0.37;
	public static final double C_WET = 1;
	public static final int SEVEN_TH = 7000;
	public static final double COEFF = 0.001;

}
