package by.gstu.diploma.drying.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DynamicCalculator {

	private static final int minutes = 60;
	private static final int secs = 60;

	private Randomizer rand;
	private final double initialPerformance;
	private volatile double totalSandProcessed;
	private volatile double currentDryerPerformance;
	private volatile double currentFinalHumidity;
	private volatile double currentStartingHumidity;
	private volatile double averagePerformance;
	private volatile List<Double> performanceHistory;

	Configuration cfg;

	private static volatile DynamicCalculator dyCalc;

	private DynamicCalculator () {
		cfg = Configuration.getInstance();

		this.currentDryerPerformance = this.averagePerformance = this.initialPerformance = cfg.getDryer().getDryerPerformance() * 1000;
		this.currentFinalHumidity = cfg.getSandHumidityAfterDrying();
		performanceHistory = new ArrayList<>();
		performanceHistory.add(this.currentDryerPerformance);
	}

	public static DynamicCalculator getInstance() {
		DynamicCalculator local = dyCalc;
		if(local == null) {
			synchronized (DynamicCalculator.class) {
				local = dyCalc;
				if(local == null) {
					dyCalc = local = new DynamicCalculator();
				}
			}
		}
		return local;
	}

	public synchronized void updateAll(int timeRate) {
		calculateAvgPerformance();
		calculateActualHumidity();
		calculateRequiredPerformance();
		updateTotalSandProcessed(timeRate);
	}

	public DynamicCalculator setRandomizer(Randomizer rand) {
		this.rand = rand;
		return this;
	}

	public double getAveragePerformance() {
		return averagePerformance;
	}

	public double getTotalSandProcessed() {
		return totalSandProcessed;
	}

	public double getCurrentFinalHumidity() {
		return currentFinalHumidity;
	}

	public double getCurrentStartingHumidity() {
		return this.currentStartingHumidity;
	}

	public double getCurrentDryerPerformance() {
		return this.currentDryerPerformance;
	}

	private void calculateAvgPerformance() {
		this.averagePerformance = this.performanceHistory.stream()
				.mapToDouble(Double::doubleValue)
				.average()
				.getAsDouble();

	}

	private void updateTotalSandProcessed(int timeRate) {
		double processedPerRate = (currentDryerPerformance
				/ minutes
				/ secs)
				* timeRate;

		totalSandProcessed += processedPerRate;
	}

	private synchronized void calculateRequiredPerformance() {
		double top = divident();
		double bot = divider();
		double newPerformance = top/bot;
		this.currentDryerPerformance = newPerformance;
		this.performanceHistory.add(this.currentDryerPerformance);
	}

	private double divident() {
		double wH = new Calculator().getwPerHour();
		double wK = cfg.getSandHumidityAfterDrying();
		return
				wH * (100 - wK);
	}

	private double divider() {
		double wN = rand.getNextRandom();
		currentStartingHumidity = wN;
		double wK = cfg.getSandHumidityAfterDrying();

		return wN - wK;
	}

	private void calculateActualHumidity() {

		Thread recalculator = new Thread(() -> {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			double wH = new Calculator().getwPerHour();
			double top = (currentDryerPerformance * currentStartingHumidity) - (wH * 100);
			double bot = currentDryerPerformance - wH;

			double newHumidity = top / bot;

			currentFinalHumidity = getNormalDistributed(currentFinalHumidity, newHumidity);
		});

		recalculator.setDaemon(true);
		recalculator.start();

	}

	private double getNormalDistributed(double from, double to) {

		if(from > to) {
			from = to + from;
			to = from - to;
			from = from - to;
		}

		boolean isTrap = false;
		double actual = 0;
		while(!isTrap) {
			double nextGauss = new Random().nextGaussian() * 100;
			if(nextGauss >= from && nextGauss <= to) {
				isTrap = true;
				actual = nextGauss;
			}
		}
		return actual;
	}

}
