package by.gstu.diploma.drying.services;

import by.gstu.diploma.drying.beans.Dryer;
import by.gstu.diploma.drying.beans.Fuel;
import by.gstu.diploma.drying.repository.SandDryRepository;

import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Configuration {

	private Double sandHumidityBeforeDrying;
	private Double sandHumidityAfterDrying;
	private Double lambda;
	private Double tempHeatbearerAtEntrance;
	private Double maxTempHeatSand;
	private Double dZero;

	private Integer zeroTemp;
	private Integer zeroHumidity;

	private Fuel fuel;

	public Fuel getFuel() {
		return fuel;
	}

	public Dryer getDryer() {
		return dryer;
	}

	private Dryer dryer;

	private static volatile Configuration cfg;

	private Configuration() {

	}

	public static Configuration getInstance() {
		Configuration localInstance = cfg;
		if(localInstance == null) {
			synchronized (Configuration.class) {
				localInstance = cfg;
				if (localInstance == null) {
					cfg = localInstance = new Configuration();
				}
			}
		}
		return localInstance;
	}

	public Integer getZeroTemp() {
		return zeroTemp;
	}

	public Integer getZeroHumidity() {
		return zeroHumidity;
	}

	public Double getLowestHeatFuelAbility() {
		return lowestHeatFuelAbility;
	}

	private Double lowestHeatFuelAbility;

	public void setDryerParams(String dryerType, Consumer<Map> callback) {
		Map<String, Object> map = new SandDryRepository().findDryerByType(dryerType);

		this.dryer = new Dryer(dryerType);
		this.dryer.mapDryerParams(map);

		callback.accept(map);
	}

	public void setLowestHeatFuelAbility(Double lowestHeatFuelAbility) {
		this.lowestHeatFuelAbility = lowestHeatFuelAbility;
	}

	public void setFuelType(String fuelType, Consumer<Map> callback) {
		this.fuel = new Fuel(fuelType);

		SandDryRepository repo = new SandDryRepository();
		Map<String, Object> map = repo.findFuelByType(fuelType);
		fuel.mapParameters(map);
		callback.accept(map);
	}

	public Double getSandHumidityBeforeDrying() {
		return sandHumidityBeforeDrying;
	}

	public void setSandHumidityBeforeDrying(Double sandHumidityBeforeDrying) {
		this.sandHumidityBeforeDrying = sandHumidityBeforeDrying;
	}

	public Double getSandHumidityAfterDrying() {
		return sandHumidityAfterDrying;
	}

	public void setSandHumidityAfterDrying(Double sandHumidityAfterDrying) {
		this.sandHumidityAfterDrying = sandHumidityAfterDrying;
	}

	public void setZeroTemp(String zeroTemp, Consumer<String> callback) {
		this.zeroTemp = Integer.parseInt(zeroTemp);

		if(zeroHumidity != null) {
			Double humidity = fetchHumidity();
			dZero = humidity;
			callback.accept(humidity.toString());
		} else
			callback.accept("Задайте влажность");
	}

	public void setZeroHumidity(String zeroHumidity, Consumer<String> callback) {
		this.zeroHumidity = Integer.parseInt(zeroHumidity);

		if(zeroTemp != null) {
			Double humidity = fetchHumidity();
			dZero = humidity;
			callback.accept(humidity.toString());
		} else
			callback.accept("Задайте температуру");
	}

	private Double fetchHumidity() {
		return new SandDryRepository()
				.findHumidityByZeroParams(zeroTemp, zeroHumidity);
	}

	public Double getLambda() {
		return lambda;
	}

	public void setLambda(Double lambda) {
		this.lambda = lambda;
	}

	public Double getTempHeatbearerAtEntrance() {
		return tempHeatbearerAtEntrance;
	}

	public void setTempHeatbearerAtEntrance(Double tempHeatbearerAtEntrance) {
		this.tempHeatbearerAtEntrance = tempHeatbearerAtEntrance;
	}

	public Double getMaxTempHeatSand() {
		return maxTempHeatSand;
	}

	public void setMaxTempHeatSand(Double maxTempHeatSand) {
		this.maxTempHeatSand = maxTempHeatSand;
	}

	public Double getDZero() {
		return dZero;
	}
}
