package by.gstu.diploma.drying.services;

import java.util.concurrent.ThreadLocalRandom;

public class Randomizer {

	private boolean isAbnormal = true;
	private int maxStandardHum = 3;
	private int abnormalHum = 83;
	private int abnormalProbability = 3;
	private final int initial;

	Randomizer() {
		this.initial = 21;//Configuration.getInstance().getSandHumidityBeforeDrying().intValue();
	}

	void setAbnormal(boolean abnormal) {
		isAbnormal = abnormal;
	}

	void setMaxStandardHum(int maxStandardHum) {
		this.maxStandardHum = maxStandardHum;
	}

	void setAbnormalHum(int abnormalHum) {
		this.abnormalHum = abnormalHum;
	}

	void setAbnormalProbability(int abnormalProbability) {
		this.abnormalProbability = abnormalProbability;
	}

	public int getNextRandom() {
		if(isAbnormal)
			return getAbnormalRandom();
		else
			return getStandardRandom();
	}

	private int getAbnormalRandom() {
		if(testProbability())
			return this.abnormalHum;
		else
			return getStandardRandom();
	}

	private int getStandardRandom() {
		return
				ThreadLocalRandom.current().nextInt(0, this.maxStandardHum) + this.initial;
	}

	private boolean testProbability() {
		int randomPercent = ThreadLocalRandom.current().nextInt(0, 99);

		return randomPercent >= 1 && randomPercent <= this.abnormalProbability;
	}
}
