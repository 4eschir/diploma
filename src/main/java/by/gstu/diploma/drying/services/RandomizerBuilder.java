package by.gstu.diploma.drying.services;

public class RandomizerBuilder {

	private Randomizer rand;

	private RandomizerBuilder () {
		rand = new Randomizer();
	}

	public static RandomizerBuilder getBuilder() {
		return new RandomizerBuilder();
	}

	public void withAbnormality(boolean isAbnormal) {
		rand.setAbnormal(isAbnormal);
	}

	public void withMaxStandardHum(int maxStandardHum) {
		rand.setMaxStandardHum(maxStandardHum);
	}

	public void withAbnormalHum(int abnormalHum) {
		rand.setAbnormalHum(abnormalHum);
	}

	public void withAbnormalProbability(int abnormalProbability) {
		rand.setAbnormalProbability(abnormalProbability);
	}

	public Randomizer build() {
		return this.rand;
	}
}
