package by.gstu.diploma.drying.services;

import by.gstu.diploma.drying.formulas.*;

import static by.gstu.diploma.drying.constants.ConstantValues.*;

public class Calculator {

	private double hZero;
	private double lZero;
	private double alpha;
	private double hP;
	private double dOne;
	private double kBig;
	private double wPerHour;
	private double qOs;
	private double tAvg;
	private double qZ;
	private double sigma;
	private double dTwo;
	private double g;
	private double gV;
	private double nUSz;
	private double nNSz;
	private double qBigOs;
	private double qBigZ;
	private double qBig;
	private double qBigIsp;

	private Formulas form;
	private Configuration cfg;
	
	public Calculator () {
	    cfg = Configuration.getInstance();
	}

	public double gethZero() {
		this.hZero = new AtmosphereAirHeatContent(cfg.getZeroTemp()).calculate();
		return hZero;
	}

	public double getlZero() {
		this.lZero = new DryAirAmmount().calculate();
		return lZero;
	}
/*
								int qRn,
                               double hP,
                               double l0,
                               double d0,
                               int t1,
                               double h0
 */
	public double getAlpha() {
		this.alpha = new ExtraAirCoefficient(
				cfg.getLowestHeatFuelAbility(),
				this.hP,
				this.lZero,
				cfg.getDZero(),
				cfg.getTempHeatbearerAtEntrance(),
				this.hZero
		).calculate();

		return alpha;
	}

	public double gethP() {
		this.hP = new AirSteamHeatContent(
				cfg.getTempHeatbearerAtEntrance()
		).calculate();

		return hP;
	}

	/*
double alpha,
double lZero,
double dZero
	 */
	public double getdOne() {
		this.dOne = new WetContainingAtEntrance(
				this.alpha,
				this.lZero,
				cfg.getDZero()
		).calculate();

		return dOne;
	}

	/*
double f,
double lambda,
	 */
	public double getkBig() {
		this.kBig = new CommonCoefficientHeatTransfer(
				cfg.getDryer().getShellThickness()/1000,
				cfg.getLambda()
		).calculate();
		return kBig;
	}

	/*
	double gH,
	double wK,
	double wH
	 */
	public double getwPerHour() {
		this.wPerHour = new SteamedWetAmmountPerHour(
				cfg.getDryer().getDryerPerformance(),
				cfg.getSandHumidityAfterDrying(),
				cfg.getSandHumidityBeforeDrying()
		).calculate();

		return wPerHour;
	}
/*
double wH,
double f,
double k,
double tAvg,
double tZero
 */
	public double getqOs() {
		this.qOs = new HeatLostPerKgWet(
				this.wPerHour,
				cfg.getDryer().getSquare(),
				this.kBig,
				this.gettAvg(),
				cfg.getZeroTemp()
		).calculate();

		return qOs;
	}

/*
	double tOne,
	double tAdd
*/
	public double gettAvg() {
		this.tAvg = new AverageHeatBearerTemp(
				cfg.getTempHeatbearerAtEntrance(),
				cfg.getMaxTempHeatSand()
		).calculate();

		return tAvg;
	}

/*
double Gh,
double cZcomma,
double tAdd,
double tZero,
double Wh
*/
	public double getqZ() {
		this.qZ = new HeatLossWithHotSandPerKgWet(
				cfg.getDryer().getDryerPerformance(),
				this.getCZComma(),
				cfg.getMaxTempHeatSand(),
				cfg.getZeroTemp(),
				this.wPerHour
		).calculate();

		return qZ;
	}

/*
double wK,
double cZ,
double cWet
*/
	public double getCZComma() {
		return new SandHeatCapacityAtOutput(
				cfg.getSandHumidityAfterDrying(),
				C_Z,
				C_WET
		).calculate();
	}

/*
double cWet,
double tOne,
double qOs,
double qZ
 */

	public double getSigma() {
		this.sigma = new IncomeLostDiffsAtOutput(
				C_WET,
				cfg.getTempHeatbearerAtEntrance(),
				this.qOs,
				this.qZ
		).calculate();
		return sigma;
	}

/*
double csv,
double tAdd,
double h1,
double sigma,
double d1
*/
	public double getdTwo() {
		this.dTwo = new HeatBearerWetContentAtOutput(
				CSV,
				cfg.getMaxTempHeatSand(),
				this.getHOne(),
				this.sigma,
				this.dOne
		).calculate();

		return dTwo;
	}

	public double getHOne() {
		return
				new AtmosphereAirHeatContent(cfg.getTempHeatbearerAtEntrance()).calculate();
	}

/*
double d2
double d1
 */
	public double getG() {
		this.g = new HeatBearerConsumptionPerOneKGSteam(
				this.dTwo,
				this.dOne
		).calculate();
		return g;
	}

/*
double g,
double h1,
double h0
 */
	public double getqV() {
		this.gV = new UnitHeatConsumptionToSteamingOneKGWet(
				this.g,
				this.getHOne(),
				this.hZero
		).calculate();

		return gV;
	}

/*
double qB,
double wH,
double wK,
double njuT
*/
	public double getnUSz() {
		this.nUSz = new UnitConditionalFuelConsumptionToSandDrying(
				this.gV,
				cfg.getSandHumidityBeforeDrying(),
				cfg.getSandHumidityAfterDrying(),
				NJU_T
		).calculate();

		return nUSz;
	}

/*
double hCondUnitToDry,
double qRn
 */
	public double getnNSz() {
		this.nNSz = new UnitNaturalFuelConsumptionToSandDrying(
				this.nUSz,
				cfg.getLowestHeatFuelAbility()
		).calculate();

		return nNSz;
	}


/*
double wH,
double qB
 */
	public double getqBigIsp() {
		this.qBigIsp = new HeatConsumptionToSteamHourly(
				this.wPerHour,
				this.gV
		).calculate();

		return qBigIsp;
	}

// second arg should be qOs, formula as in previous method
	public double getqBigOs() {
		this.qBigOs = new HeatConsumptionToSteamHourly(
				this.wPerHour,
				this.qOs
		).calculate();

		return qBigOs;
	}

// second arg should be qZ, formula as in previous method
	public double getqBigZ() {
		this.qBigZ = new HeatConsumptionToSteamHourly(
				this.wPerHour,
				this.qZ
		).calculate();

		return qBigZ;
	}

/*
double gH,
double hCondUnitToDry,
double cT,
double qRn
 */
	public double getqBig() {
		this.qBig = new PowerDryingMachineDefiner(
				cfg.getDryer().getDryerPerformance(),
				this.nUSz,
				C_T,
				cfg.getLowestHeatFuelAbility()
		).calculate();

		return qBig;
	}


}
