package by.gstu.diploma.drying.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoSocketOpenException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.HashMap;
import java.util.Map;

public class SandDryRepository {

	private ObjectMapper mapper;
	private final String dbName = "SandDrying";
	private final String dryerTypesCollectionName = "DryerTypes";
	private final String humidityCollectionName = "AirHumidity";
	private final String fuelCollectionName = "GasContent";

	MongoDatabase dataBase = new MongoClient().getDatabase(dbName);

	public Map<String, Object> findDryerByType(String type) {
		mapper = new ObjectMapper();
		Map<String, Object> map = new HashMap<>();
		Document search = new Document("name", type);
		Document found = dataBase.getCollection(dryerTypesCollectionName).find(search).first();
		try {
			map = mapper.readValue(found.toJson(), Map.class);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return map;
	}

	public Double findHumidityByZeroParams(int temp, int hum) {
		mapper = new ObjectMapper();
		Map<String, Object> map = new HashMap<>();
		Document search = new Document("relHum", hum);
		Document doc = dataBase.getCollection(humidityCollectionName).find(search).first();
		if(doc != null) {
			try {
				map = mapper.readValue(doc.toJson(), Map.class);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		return
				(Double) map.get(String.valueOf(temp));
	}

	public Map<String, Object> findFuelByType(String fuelType) {
		mapper = new ObjectMapper();
		Map<String, Object> map = new HashMap<>();
		Document search = new Document("name", fuelType);
		Document doc = dataBase.getCollection(fuelCollectionName).find(search).first();

		if(doc != null) {
			try {
				map = mapper.readValue(doc.toJson(), Map.class);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		return map;
	}
}
